<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiPembelianBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transaksi_pembelian_barangs');
        Schema::create('transaksi_pembelian_barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaksi_pembelian_id');
            $table->unsignedInteger('master_barang_id');
            $table->integer('jumlah');
            $table->float('harga_satuan', 8, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pembelian_barangs');
    }
}
