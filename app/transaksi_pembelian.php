<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian extends Model
{
    public function transaksiPembelianBarang()
    {
        return $this->hasMany('App\transaksi_pembelian_barang');
    }
}
