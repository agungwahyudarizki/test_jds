<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian_barang extends Model
{
    public function transaksiPembelian()
    {
        return $this->belongsTo('App\transaksi_pembelian');
    }

    public function masterBarang()
    {
        return $this->belongsTo('App\master_barang');
    }
}
