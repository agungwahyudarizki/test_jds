<?php

namespace App\Http\Controllers;

use App\master_barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = master_barang::all();
        return view('barang', ['barang'=> $barang]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('master_barangs')->insert([
            'nama_barang' => $request->nama,
            'harga_satuan' => $request->harga
        ]);

        return redirect('/master-barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function show(master_barang $master_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(master_barang $master_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, master_barang $master_barang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(master_barang $master_barang)
    {
        //
    }
}
