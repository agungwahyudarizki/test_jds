<?php

namespace App\Http\Controllers;

use App\transaksi_pembelian_barang;
use Illuminate\Http\Request;

class TransaksiPembelianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = transaksi_pembelian_barang::all();
        return view('transaksi', ['transaksi' => $transaksi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaksi-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function show(transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }
}
