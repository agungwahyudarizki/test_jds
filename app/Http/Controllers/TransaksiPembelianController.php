<?php

namespace App\Http\Controllers;

use App\transaksi_pembelian;
use Illuminate\Http\Request;

class TransaksiPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function show(transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function edit(transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function destroy(transaksi_pembelian $transaksi_pembelian)
    {
        //
    }
}
