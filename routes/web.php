<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master-barang', 'MasterBarangController@index');
Route::get('/master-barang/create', 'MasterBarangController@create');
Route::post('/master-barang/store', 'MasterBarangController@store');
Route::get('/transaksi', 'TransaksiPembelianBarangController@index');
Route::get('/transaksi/create', 'TransaksiPembelianBarangController@create');
