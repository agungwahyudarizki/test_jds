<!DOCTYPE html>
<html>
<head>
	<title>Aplikasi Kasir</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
	<div class="container">
		<div class="card">
			<div class="card-body">
				<h3>Transaksi</h3>
				<a href="/transaksi">Kembali</a>
				<br/>
				<br/>
				<form action="/transaksi/store" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                            <label>Barang</label>
                            <select class="form-control">
                            </select>
                          </div>
					<div class="form-group">
						<label>Kuantitas</label>
						<input type="text" class="form-control" name="kuantitas" required="required">
                    </div>
                    <div class="form-group">
                        <label>Subtotal</label>
                        <input type="text" class="form-control" name="subtotal" required="required">
                    </div>
					<input type="submit" value="Simpan Data" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</body>
</html>