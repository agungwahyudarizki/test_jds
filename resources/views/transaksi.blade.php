<!DOCTYPE html>
<html>
    <head>
        <title>Aplikasi Kasir</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <a href="/">Kembali</a>
                    <h3>Data Transaksi</h3>
                    <a href="/transaksi/create">Transaksi Baru</a>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Id Barang</th>
                            <th>Id Transaksi</th>
                            <th>Jumlah</th>
                            <th>Harga Satuan</th>
                        </tr>
                        @foreach($transaksi as $t)
                        <tr>
                            <td>{{ $t->id }}</td>
                            <td>{{ $t->master_barang_id }}</td>
                            <td>{{ $t->transaksi_pembelian_id }}</td>
                            <td>{{ $t->jumlah }}</td>
                            <td>{{ $t->harga_satuan }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>