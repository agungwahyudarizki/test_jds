<!DOCTYPE html>
<html>
    <head>
        <title>Aplikasi Kasir</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <a href="/">Kembali</a>
                    <h3>Master Barang</h3>
                    <a href="/master-barang/create">Tambah Barang Baru</a>
                    <br/><br/>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Harga</th>
                        </tr>
                        @foreach($barang as $b)
                        <tr>
                            <td>{{ $b->id }}</td>
                            <td>{{ $b->nama_barang }}</td>
                            <td>{{ $b->harga_satuan }}</td>

                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>