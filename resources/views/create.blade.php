<!DOCTYPE html>
<html>
<head>
	<title>Aplikasi Kasir</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
	<div class="container">
		<div class="card">
			<div class="card-body">
				<h3>Master Barang</h3>
				<a href="/master-barang">Kembali</a>
				<br/>
				<br/>
				<form action="/master-barang/store" method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<label>Nama Barang</label>
						<input type="text" class="form-control" name="nama" required="required">
					</div>
					<div class="form-group">
						<label>Harga Barang</label>
						<input type="number" class="form-control" name="harga" required="required">
					</div>
					<input type="submit" value="Simpan Data" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</body>
</html>